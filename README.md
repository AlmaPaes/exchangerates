# Práctica Extra 1

## Autora 
Alma Rosa Páes Alcalá.  
*paes@ciencias.unam.mx*


## Elementos adicionales utilizados
**JSON Server:** [Documentación](https://www.npmjs.com/package/json-server)

## Instrucciones para ejecutar
1. Situarse en la carpeta raíz del proyecto
2. Correr servicio back-end: `npm run api`
3. En otra termina, correr servicio front-end: `ng serve`
