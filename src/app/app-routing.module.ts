import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MonedaComponent } from './_components/moneda/moneda.component';


const routes: Routes = [
  { path: '', component: MonedaComponent},
  { path: 'rates', component: MonedaComponent},
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
