import { Tarifa } from './tarifa';

export class Moneda{
    id: number;
    base: String;
    date: Date;
    time_last_updated: number;
    rates: Tarifa[];

    constructor(id,base, date, time_last_updated, rates){
        this.id = id;
        this.base = base;
        this.date = date;
        this.time_last_updated = time_last_updated;
        this.rates = rates;
    }
} 