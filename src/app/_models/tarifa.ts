export class Tarifa{
    nombre: String;
    rate: number;

    constructor(nombre, rate){
        this.nombre = nombre;
        this.rate = rate;
    }
}