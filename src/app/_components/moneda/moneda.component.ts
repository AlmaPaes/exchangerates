import { Component, OnInit } from '@angular/core';
import { Moneda } from 'src/app/_models/moneda';
import { MonedaService } from 'src/app/_services/moneda.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-moneda',
  templateUrl: './moneda.component.html',
  styleUrls: ['./moneda.component.css']
})
export class MonedaComponent implements OnInit {

  moneda: Moneda | any;
  tipo_moneda = 'MXN';

  constructor(private monedaService: MonedaService) { }

  ngOnInit(): void {
    this.getRates();
  }

  onSubmit(){
    this.monedaService.getRates(this.tipo_moneda.toUpperCase()).subscribe(
      res => {
        if (res[0] == null){
          Swal.fire(
            'Lo lamento!',
            'No tenemos datos de esa moneda ',
            'warning'
          )
        } else{
          this.moneda = res[0];
        }        
      },
      err => console.error(err)
    )
  }

  getRates(){
    this.moneda = [];
    this.monedaService.getRates(this.tipo_moneda).subscribe(
      res => {
        this.moneda = res[0];
      },
      err => console.error(err)
    )
  }

}
